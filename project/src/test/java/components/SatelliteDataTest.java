package components;

import static org.junit.Assert.assertThrows;

import org.junit.Test;

public class SatelliteDataTest {

    SatelliteData test;

    @Test
    public void ilegalInputData() {

        assertThrows(IllegalArgumentException.class, () -> {
            test = new SatelliteData("20180101 23:01:05.001|1001T|101|98|25|20|99.9|TSTAT");
        });
    }
    
}
