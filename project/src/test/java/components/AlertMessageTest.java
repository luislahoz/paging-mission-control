package components;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

import java.text.ParseException;

import org.junit.Test;

public class AlertMessageTest {

    AlertMessage test;

    @Test
    public void severityTest(){
        try {
            test = new AlertMessage(1000, "BAT", "20180101 23:01:05.001");

            assertEquals(null, test.severity);

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void parsingTest(){
        assertThrows(ParseException.class, () -> {
            test = new AlertMessage(1000, "BATT", "20180101 T 23:01:05.001");
        });
    }
    
}
