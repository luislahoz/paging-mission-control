import java.util.ArrayList;
import java.util.Date;
import java.util.ListIterator;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import components.AlertMessage;
import components.SatelliteData;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class PagingMissionControl {

    public static void main(String[] args) throws FileNotFoundException, ParseException {

        // Path to files needs to be updated
        Scanner scanner = new Scanner(new FileReader("/opt/dataSample.txt"));

        // If for the same satellite there are three battery voltage readings that are
        // under the red low limit within a five minute interval.
        ArrayList<SatelliteData> redLowList = new ArrayList<SatelliteData>();
        // If for the same satellite there are three thermostat readings that exceed the
        // red high limit within a five minute interval.
        ArrayList<SatelliteData> redHighList = new ArrayList<SatelliteData>();

        // JSON converter
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        // Alert Messages list
        ArrayList <AlertMessage> alertList = new ArrayList<AlertMessage>();

        while (scanner.hasNextLine()) {
            // Read from file one line at a time
            SatelliteData satelliteData = new SatelliteData(scanner.nextLine());

            // There are multiple ways to get this, using component or calculating it, i
            // went the route to calculating it
            // I wanted to remove the yellow warnings with the red limits
            if (satelliteData.getRawValue() < satelliteData.getRedLowLimit()) {
                AlertMessage alert = createAlert(satelliteData, redLowList);
                if(alert != null){
                    alertList.add(alert);
                    // jsonString = gson.toJson(alert);
                    // System.out.println(jsonString);
                }
                redLowList.add(satelliteData);
            }
            if (satelliteData.getRawValue() > satelliteData.getRedHighLimit()) {
                AlertMessage alert = createAlert(satelliteData, redHighList);
                if(alert != null){
                    alertList.add(alert);
                    // jsonString = gson.toJson(alert);
                    // System.out.println(jsonString);
                }
                redHighList.add(satelliteData);
            }
        }

        // Generate json
        String jsonString = gson.toJson(alertList, ArrayList.class);

        System.out.println(jsonString);
    }

    private static AlertMessage createAlert(SatelliteData data, ArrayList<SatelliteData> dataList) throws ParseException {

        int count = 0;
        long fiveMinutes = 5 * 60 * 1000;
        long currentTime = getTimeInEpoch(data.getTimestamp());
        long historyTime;

        // Reverse iterator, with new alert check in history list if there were other alerts within 5 minutes
        for (ListIterator<SatelliteData> iterator = dataList.listIterator(dataList.size()); iterator.hasPrevious();) {
            SatelliteData satelliteData = (SatelliteData) iterator.previous();
            historyTime = getTimeInEpoch(satelliteData.getTimestamp());

            // Compare newReading with historic readings are within 5 minutes and that they come from the same satelliteId
            if ((currentTime - historyTime <= fiveMinutes) && data.getSatelliteId().equals(satelliteData.getSatelliteId())) {
                count++;
                if (count >= 2) {
                    // Create message with SatelliteData information
                    return new AlertMessage(satelliteData.getSatelliteId(), satelliteData.getComponent(), satelliteData.getTimestamp());
                }
            }
        }

        return null;

    }

    private static Long getTimeInEpoch(String time) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
        Date date = df.parse(time);
        long epoch = date.getTime();
        return epoch;
    }

}