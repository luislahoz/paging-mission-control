package components;

public class SatelliteData {

    private String timestamp;
    private Integer satelliteId;
    private Double redHighLimit;
    private Double yellowHighLimit;
    private Double yellowLowLimit;
    private Double redLowLimit;
    private Double rawValue;
    private String component;
    
    public SatelliteData (String data){
        //Break down the input data
        String[] dataArray = data.split("\\|");
        timestamp = dataArray[0];
        satelliteId = Integer.parseInt(dataArray[1]);
        redHighLimit = Double.parseDouble(dataArray[2]);
        yellowHighLimit = Double.parseDouble(dataArray[3]);
        yellowLowLimit = Double.parseDouble(dataArray[4]);
        redLowLimit = Double.parseDouble(dataArray[5]);
        rawValue = Double.parseDouble(dataArray[6]);
        component = dataArray[7];
    }
    
    // Getters
    public String getTimestamp() {
        return timestamp;
    }

    public Integer getSatelliteId(){
        return satelliteId;
    }

    public Double getRedHighLimit() {
        return redHighLimit;
    }

    public Double getYellowHighLimit() {
        return yellowHighLimit;
    }

    public Double getYellowLowLimit() {
        return yellowLowLimit;
    }
    public Double getRedLowLimit() {
        return redLowLimit;
    }

    public Double getRawValue() {
        return rawValue;
    }

    public String getComponent() {
        return component;
    }
    



}