package components;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AlertMessage {
    
    Integer satelliteId;
    String severity;
    String component;
    String timestamp;


    public AlertMessage(Integer satelliteId, String component, String timestamp) throws ParseException {
        this.satelliteId = satelliteId;
        this.severity = getSeverity(component);
        this.component = component;
        this.timestamp = getFormattedDateTime(timestamp);
    }

    // Get Severity from component
    private String getSeverity(String component){
        switch (component){
            case "TSTAT": return "RED HIGH";
            case "BATT": return "RED LOW";
            default: return null;
        }
    }

    // get Format as requested
    private String getFormattedDateTime(String timestamp) throws ParseException{
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
        SimpleDateFormat alertFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:MM:ss:SSS'Z'");
        Date date = df.parse(timestamp);
        String formattedTime = alertFormat.format(date);
        return formattedTime;
    }

}
